export function Todo(id,title,completed = false) {
    this.id = id
    this.title = title
    this.completed = completed
}