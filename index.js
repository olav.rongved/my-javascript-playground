import { Todo } from './todo.js'



function App(){
    // Variables

    
    this.todoCtrl = new TodoController()

    // DOM Elements
    this.elNewTodoTitle = document.getElementById('new-todo-title')
    this.elBtnAddTodo   = document.getElementById('btn-add-todo')
    this.elTodoList     = document.getElementById('todo-list')

    this.init = function(){
        //Initialize Event Listeners

        // HTML Events
        this.elBtnAddTodo.addEventListener('click',
                                            this.handleAddTodoClick.bind(this))
        this.render()
    }
    //Event handler
    this.handleAddTodoClick = function (){
        
        const newTodo = this.elNewTodoTitle.value.trim();


        //Add to array
        this.todoCtrl.addTodo( newTodo )
        this.render()

    }
    this.render = function (){

        this.elTodoList.innerHTML = ''
        this.todoCtrl.createTodoItems()
            .forEach((todoItem) => {
                this.elTodoList.appendChild(todoItem)

        })
    }
}

function TodoController() {

    this.todos = [new Todo(1,'test')];

    console.log(this.todos);
    this.addTodo = function(title) {
        const id = this.todos[this.todos.length - 1].id + 1;
        this.todos.push(new Todo(id,title))
    }
    // Return list of HTML elements.
    this.createTodoItems = function() {
        const elTodoItems = this.todos.map(function(todo){
            const elTodoItem = document.createElement('li')
            elTodoItem.innerText = todo.title;
            return elTodoItem
        })
        return elTodoItems
    }
}

new App().init()









